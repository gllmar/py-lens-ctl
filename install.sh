#!/bin/bash

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

BINARY_NAME=py-lens-ctl.py

echo "installing rpi-adamotor-osc server to /usr/local/bin"
	sudo ln -s $SCRIPTPATH/src/$BINARY_NAME /usr/local/bin/$BINARY_NAME
	sudo chmod +x /usr/local/bin/$BINARY_NAME

echo "installing systemd services"


for filename in $SCRIPTPATH/systemd/*.service; do
    [ -e "$filename" ] || continue
	echo "processing $filename"
	sudo cp $filename /etc/systemd/system/
	SERVICE=${filename##*/} # (get only file without basename :: posix compliant :)
	sudo systemctl daemon-reload
	sudo systemctl start $SERVICE
	sudo systemctl enable $SERVICE
done

