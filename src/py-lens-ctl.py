#!/usr/bin/env python

## import osc related libs
from __future__ import print_function
from liblo import *
import sys

## import timing
import time
import threading

## import serial
import os
import serial
from serial import SerialException

## declare globals
global_debug=0
global_zero_flag=0
global_absolute_focus=0 #
global_absolute_zoom=0
global_absolute_focus_old=0
global_absolute_zoom_old=0
global_motor_speed=0
global_motor_speed_flag=0
global_motor_power=0
global_motor_power_flag=0
global_motor_init_flag=0

# OSC_SERVER class

class OSC_server(ServerThread):
	def __init__(self):
		ServerThread.__init__(self, 2000)
		print("serving OSC on 2000")

	@make_method('/absolute/focus', 'i')
	def absolute_focus_callback(self, path, args):
		global global_absolute_focus
		global_absolute_focus, = args
		if global_debug!=0:
			print ("%s %f "%(path, global_absolute_focus))

	@make_method('/absolute/zoom', 'i')
	def absolute_zoom_callback(self, path, args):
		global global_absolute_zoom
		global_absolute_zoom, = args
		if global_debug!=0:
			print ("%s %f "%(path, global_absolute_zoom))

	@make_method('/zero', 'i')
	def zero_callback(self, path, args):
		global global_zero_flag
		global_zero_flag, = args
		if global_debug!=0:
			print ("%s %f "%(path, global_zero_flag))

	@make_method('/motor/speed', 'i')
	def motor_speed(self, path, args):
		global global_motor_speed
		global global_motor_speed_flag
		global_motor_speed, = args
		global_motor_speed_flag=1
		if global_debug!=0:
			print ("%s %f "%(path, global_motor_speed))

	@make_method('/motor/power', 'i')
	def motor_power(self, path, args):
		global global_motor_power
		global global_motor_power_flag
		global_motor_power, = args
		global_motor_power_flag=1
		if global_debug!=0:
			print ("%s %f "%(path, global_motor_power))

	@make_method('/motor/init', 'i')
	def motor_init(self, path, args):
		global global_motor_init_flag
		global_motor_init_flag=1

	@make_method(None, None)
	def fallback(self, path, args):
		print("received unknown message '%s'" % path)

# Define Serial Thread

class serial_thread( threading.Thread ):
	serial_response_flag=1
	serial_response_bytes_old=0
	reset_motor_flag=1
	x_value=-1
	y_value=-1
	t_value=-1
	p_value=-1
	c1_value=-1
	c2_value=-1

	def __init__(self, threadID, name ):
		threading.Thread.__init__(self)
		self.name=name
		self.threadID=threadID
		self.serial_init_connection()

	def serial_init_connection(self):
		if os.path.exists('/dev/ttyUSB0'):
			self.ser = serial.Serial(port='/dev/ttyUSB0',baudrate = 115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS,timeout=1)
			self.reset_motor_flag=1
		elif os.path.exists('/dev/ttyUSB1'):
			self.ser = serial.Serial(port='/dev/ttyUSB1',baudrate = 115200,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS,timeout=1)
			self.reset_motor_flag=1
		else:
			print("serial error out of bound")

	def is_serial_connected(self):
		'''should return True if usb serial is connected'''
		if os.path.exists('/dev/ttyUSB0') or os.path.exists('/dev/ttyUSB1'):
			return True
		else:
			return False
			print("error serial not connected")
			serial_init_connection()

	def send_serial(self, cmd):
		'''send serial formated command to USB serial '''
		try :
			self.ser.write(bytes(cmd, 'utf8'))
		except :
			print("Serial disconected; reconnecting")
			self.ser.close()
			serial_init_connection()

	def move_motor_to(self, focus_to,zoom_to):
		'''Move motor position to X and Y (0 to 32000/64000(?))'''
		global global_absolute_focus_old
		global global_absolute_focus
		global global_absolute_zoom_old
		global global_absolute_zoom
		cmd = 'G0 X%d Y%d\n' % (focus_to, zoom_to,)
		global_absolute_focus_old=global_absolute_zoom=focus_to
		global_absolute_zoom_old=global_absolute_zoom=zoom_to
		self.send_serial(cmd)

	def set_motor_to(self,focus_set,zoom_set):
		'''should set motor position to x,y without moving the motor (zero?)'''
		global global_zero_flag
		cmd = 'G92 X%d Y%d\n' % (focus_set, zoom_set,)
		self.send_serial(cmd)
		global_zero_flag=0
		print("set_motor_to:%d,%d"%(focus_set,zoom_set))

	def set_motor_speed_to(self,speed):
		global global_motor_speed
		global global_motor_speed_flag
		'''should set motor speed 100 to 10000 (?) '''
		cmd = 'M99 R%d \n' % (global_motor_speed,)
		global_motor_speed_flag=0
		print("setting motor speed:" )
		self.send_serial(cmd)

	def set_motor_power_to(self,power):
		global global_motor_power
		global global_motor_power_flag
		'''should set motor power 1 to 100(?)'''
		cmd = 'M98 R%d \n' % (global_motor_power,)
		global_motor_power_flag=0
		print("setting motor power")
		print(global_motor_power)
		self.send_serial(cmd)

	def check_reset_motors_limits(self):
		'''sequence to init zero on motors '''
		if self.reset_motor_flag>0:
			global global_absolute_focus
			global global_absolute_zoom
			actual_focus=self.x_value
			actual_zoom=self.y_value
			flag=self.reset_motor_flag
			focus_limits=[0, 64000, 0, 4200, 42588]
			zoom_limits= [0, 64000, 0,32728,  3294]
			if flag==1:
				global_absolute_focus=focus_limits[0]
				global_absolute_zoom=zoom_limits[0]
				self.reset_motor_flag=2
				print("flag1")
			for i in range(1,len(focus_limits)):
				if flag==i+1 and actual_focus==focus_limits[i-1] and actual_zoom==zoom_limits[i-1]:
					global_absolute_focus=focus_limits[i]
					global_absolute_zoom=zoom_limits[i]
					self.reset_motor_flag=flag+1
			if flag==len(focus_limits)+1 and actual_focus==focus_limits[len(focus_limits)-1] and actual_zoom==zoom_limits[len(focus_limits)-1]:
				self.reset_motor_flag=0
				print("init complete")

	def read_serial(self):
		'''should read message on serial and parse x y t p c1 c2 to self vars'''
		try :
			while self.ser.in_waiting:
				line=self.ser.readline()
				if line!=self.serial_response_bytes_old:
					self.serial_response_bytes_old=line
					line=line.decode()
					line=line.strip("\n")
					line=line.strip("\r")
					line=line.replace("=", ",")
					line=line.split(",")
					if len(line)==13: #expected response array length
						self.x_value=int(line[2])
						self.y_value=int(line[4])
						self.t_value=int(line[6])
						self.p_value=int(line[8])
						self.c1_value=int(line[10])
						self.c2_value=int(line[12])
					if self.serial_response_flag:
						#print(self.x_value,self.y_value,self.t_value,self.p_value,self.c1_value,self.c2_value)
						pass

		except:
			print("ERROR unable to read serial reconnecting")
			time.sleep(.4) #need, time else crash
			self.ser.close()
			time.sleep(.1) #need, time else crash
			self.serial_init_connection()

	def run(self):
		while True:
			global global_absolute_focus
			global global_absolute_zoom
			global global_absolute_focus_old
			global global_absolute_zoom_old
			global global_zero_flag
			global global_motor_init_flag
			global global_motor_speed_flag
			global global_motor_power_flag
			global global_motor_speed
			global global_motor_power

			if self.is_serial_connected:
				self.check_reset_motors_limits()
				if global_absolute_focus!=global_absolute_focus_old or global_absolute_zoom!=global_absolute_zoom_old:
					self.move_motor_to(global_absolute_focus, global_absolute_zoom)
				elif global_zero_flag:
					self.set_motor_to(0,0)
				elif global_motor_init_flag:
					global_motor_init_flag=0
					self.reset_motor_flag=1	
				elif global_motor_speed_flag:
					self.set_motor_speed_to(global_motor_speed)
				elif global_motor_power_flag:
					self.set_motor_power_to(global_motor_power)
				else:
					self.read_serial()
					time.sleep(0.001)


print("init_start")

try:
	server = OSC_server()
except ServerError as err:
	print(err)
	sys.exit()

try:
	serial_ctl = serial_thread(1 ,1)
except:
	print("serial error")

server.start()
serial_ctl.start()

print("init_end; running")
