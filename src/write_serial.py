#!/usr/bin/env python
import time
import serial
ser = serial.Serial(
	port='/dev/ttyUSB0',
	baudrate = 115200,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS,
	timeout=1
)
counter=0
while 1:
	#ser.write('Write counter: %d \n'%(counter))
	cmd = 'G0 X%d Y%d\n' % (40000, 4000,)
	ser.write(bytes(cmd, 'utf8'))
	time.sleep(1)
	counter += 1000
	counter = counter%42000
	print(cmd)
